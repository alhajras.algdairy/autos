# Generated by Django 3.0.8 on 2020-11-25 19:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("catalog", "0014_auto_20201125_1910"),
    ]

    operations = [
        migrations.AlterField(
            model_name="image",
            name="image",
            field=models.ImageField(upload_to="catalog/static/images"),
        ),
        migrations.AlterField(
            model_name="image",
            name="vehicle",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="images",
                to="catalog.Vehicle",
            ),
        ),
    ]
